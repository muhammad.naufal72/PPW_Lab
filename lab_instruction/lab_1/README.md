# Lab 1: Introduction to Git (on GitLab) & TDD (Test-Driven Development) with Django

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## Learning Objectives

After completing this exercise, students will be able to:

- Remember essential Git commands for individual work
- Demonstrate essential Git commands for individual work
- Setup a local and online (GitLab) Git repository
- Setup a remote between local Git repository and its counterpart on GitLab
- Practice basic TDD development cycle
- Publish his/her work on a PaaS (Platform-as-a-Service) cloud service provider


## Checklist

1. Creating Your own Gitlab Repo
    1. [ ] Write your Gitlab Repo link in README.md
2. Your web that deployed in Heroku Instance
    1. [ ] Create your Heroku Account
    2. [ ] Write your apps link (`######`.herokuapp.com) in README.md
3. Creating a functionality in Django framework and content in HTML
    1. [ ] Implement a new function to calculate age
    2. [ ] Calculate your age by passing your birth year into the function
    3. [ ] Pass the return value from age calculation into the template HTML
    4. [ ] The return value is rendered within an `<article>` HTML5 tag
4. Check your pipelines.
    1. [ ] Make sure the test stage is success, check the console by clicking the unit test. Makesure the coverage is 100%
    2. [ ] Make sure the deployment script has successfully deploy your webpage to heroku. 
    Check if there are some warning messages in the console.

